import { StaffNowAppPage } from './app.po';

describe('staff-now App', () => {
  let page: StaffNowAppPage;

  beforeEach(() => {
    page = new StaffNowAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
