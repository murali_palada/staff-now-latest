import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Popover } from 'ngx-popover';
import classie from 'js/textbox.js';
import { HomeService } from '../../home/home.service';
import { FormGroup, FormBuilder, Validators, NgForm, FormControl } from "@angular/forms";
import { Resources } from '../resources/resources';

@Component({
  selector: 'interested-manager-popover',
  templateUrl: '../../home/popovers/interested-manager-popover.component.html'
})
export class InterestedManagerPopoverComponent implements OnInit {
  apiUrl: string;
  private interestedManagerForm: FormGroup;
  showErrorFlash: boolean = false;
  errorMessage: string = "";
  public resources: Resources;

  @Input() employeeId: string;
  @Input() interestedListLength: number;
  @Input() managerEmailId: string;
  @Input() employeeName: string;
  @Output() refreshParentBenchEmployeesList$: EventEmitter<string> = new EventEmitter();

  @ViewChild('popoverInterestedForm') public popoverInterestedForm: Popover;
  @ViewChild('popoverInterestedPeople') public popoverInterestedPeople: Popover;

  interestedManagerDetails: any = [];

  constructor(
    private homeService: HomeService,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.apiUrl = "http://180.151.38.87:3000";
    this.buildForm();
    this.initializeClassieJS();
    this.resources = new Resources();
  }

  buildForm() {
    this.interestedManagerForm = this.formBuilder.group({
      interestedManagerName: ['', Validators.required],
      interestedManagerEmail: ['', [Validators.required, Validators.pattern]],
      interestedManagerComments: []
    });

    this.interestedManagerForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.interestedManagerForm) { return; }
    const form = this.interestedManagerForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  formErrors = {
    'interestedManagerName': '',
    'interestedManagerEmail': ''
  };

  validationMessages = {
    'interestedManagerName': {
      'required': 'Manager Name is required.'
    },
    'interestedManagerEmail': {
      'required': 'Manager Email Id is required.',
      'pattern': 'Invalid Email Id.'
    }
  };

  initializeClassieJS() {
    (function () {
      // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
      if (!String.prototype.trim) {
        (function () {
          // Make sure we trim BOM and NBSP
          var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
          String.prototype.trim = function () {
            return this.replace(rtrim, '');
          };
        })();
      }

      [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
        // in case the input is already filled..
        if (inputEl.value.trim() !== '') {
          classie.add(inputEl.parentNode, 'input--filled');
        }

        // events:
        inputEl.addEventListener('focus', onInputFocus);
        inputEl.addEventListener('blur', onInputBlur);
      });

      function onInputFocus(ev) {
        classie.add(ev.target.parentNode, 'input--filled');
      }

      function onInputBlur(ev) {
        if (ev.target.value.trim() === '') {
          classie.remove(ev.target.parentNode, 'input--filled');
        }
      }
    })();
  }

  showPopoverInterestedForm() {
    this.popoverInterestedForm.show();
  }

  closePopoverInterestedForm() {
    this.popoverInterestedForm.hide();
    this.buildForm();
  }

  showPopoverInterestedPeople() {
    let searchViewModel: any;

    searchViewModel = [{
      "empId": this.employeeId
    }];

    this.homeService.getInterestedManagerDetails(this.apiUrl + "/getinterestedlist", searchViewModel).subscribe(data => {
      this.interestedManagerDetails = [];
      this.interestedManagerDetails = data;
    },
      err => {
        this.showErrorFlash = true;
        this.errorMessage = this.resources.errorFetchIntrstdMngrDtls;
        setTimeout(() => {
          this.hideErrorFlash();
        }, 6000);
      },
      () => {
        this.popoverInterestedPeople.show();
      });
  }

  saveInterestedManagerDetails() {
    let viewModel = [{
      "empId": this.employeeId,
      "interestedManagerName": this.interestedManagerForm.value.interestedManagerName,
      "interestedManagerEmailId": this.interestedManagerForm.value.interestedManagerEmail,
      "interestedManagerComment": this.interestedManagerForm.value.interestedManagerComments
    }];

    this.homeService.saveInterestedManagerDetails(this.apiUrl + "/interestedlist", viewModel).subscribe(data => {
      this.sendEmail();
    },
      err => {
        this.showErrorFlash = true;
        this.errorMessage = this.resources.errorSaveIntrtdManagrDtls;
        setTimeout(() => {
          this.hideErrorFlash();
        }, 6000);
      },
      () => {
        this.closePopoverInterestedForm();
        this.refreshParentBenchEmployeesList$.emit();
      });
  }

  hideErrorFlash() {
    this.showErrorFlash = false;
    this.errorMessage = "";
  }

  sendEmail() {
    let viewModel = [{
      "from": this.interestedManagerForm.value.interestedManagerEmail,
      "to": this.managerEmailId,
      "subject": "Staff Now : Interest notification on " + this.employeeName + " Profile.",
      "text": "",
      "html": "Hi " + this.interestedManagerForm.value.interestedManagerName + "," + "<br/><br/>I'm interested in " + this.employeeName + " Profile."
    }];
    this.homeService.sendEmail(this.apiUrl + "/sendMail", viewModel).subscribe(data => {
    },
      err => {
      },
      () => {
      });
  }
}