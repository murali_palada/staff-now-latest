import { DatePickerOptions, DateModel } from 'ng2-datepicker';

export class DatePickerModel {
    availableByDate: DateModel;
    availableByDatePickerOptions: DatePickerOptions;

    constructor() {
        this.availableByDate = new DateModel();
        this.availableByDate.formatted = "";
        this.availableByDatePickerOptions = new DatePickerOptions();
        this.availableByDatePickerOptions.format = "DD-MM-YYYY";
        this.availableByDatePickerOptions.initialDate = new Date();
    }
}

export class PaginationModel {
    // pager object
    pager: any;
    // paged items
    pagedItems: any[];

    public from: number;
    public to: number;
    public total: number;

    constructor() {
        this.pager = {};
        this.pagedItems = [];
        this.from = 0;
        this.to = 0;
        this.total = 0;
    }
}

export class SearchEmployeeViewModel {
    public name: string;
    public skills: string;
    public location: string;
    public company: string;

    constructor() {
        this.name = "";
        this.skills = "";
        this.location = "";
        this.company = "";
    }
}

export class EmployeeViewModel {
    private employeeId: string;
    private name: string;
    private lastReportingManager: string;
    private location: string;
    private company: string;
    private phone: string;
    private email: string;
    public skills: string[];
    public stringSkills: string;
    public resume: string;
    public availableBy: string;

    constructor() {
        this.employeeId = "";
        this.name = "";
        this.lastReportingManager = "";
        this.location = "";
        this.company = "";
        this.phone = "";
        this.email = "";
        this.skills = [];
        this.stringSkills = "";
        this.resume = "";
        this.availableBy = "";
    }
}