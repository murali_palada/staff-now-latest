import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { HomeService } from "./home.service";
import { ModalDirective } from 'ng2-bootstrap';
import * as _ from 'underscore';
import { PagerService } from './pager-service/pager.service';
import classie from 'js/textbox.js';
import { DatePickerModel, PaginationModel, SearchEmployeeViewModel, EmployeeViewModel } from './models/viewmodels';
import { Popover } from 'ngx-popover';
import { FormGroup, FormBuilder, Validators, NgForm, FormControl } from "@angular/forms";
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import { Resources } from './resources/resources';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit {
    apiUrl: string;
    private addEmployeeForm: FormGroup;
    showSuccessFlash: boolean;
    successMessage: string;
    showErrorFlash: boolean;
    errorMessage: string;
    showSpinner: boolean;
    availableByDateModel: DatePickerModel;
    searchEmployeeViewModel: SearchEmployeeViewModel;
    paginationModel: PaginationModel;

    private benchEmployees: any = [];
    private locations: any = [];
    private companies: any = [];
    private searchLocations: any = [];
    private searchCompanies: any = [];

    filesToUpload: any;

    public showHideAdvancedSearchDiv: boolean;
    public technology: string;
    public resources: Resources;

    @ViewChild('addEmployeeModal') public addEmployeeModal: ModalDirective;
    @ViewChild('successModal') public successModal: ModalDirective;
    @ViewChild('fileInput') public fileInputVariable: any;
    @ViewChild('popoverInterestedForm') public popoverInterestedForm: Popover;
    @ViewChild('popoverInterestedPeople') public popoverInterestedPeople: Popover;
    @ViewChild('popoverShortlistedForm') public popoverShortlistedForm: Popover;
    @ViewChild('popoverShortlistedPeople') public popoverShortlistedPeople: Popover;

    constructor(
        private homeService: HomeService,
        private pagerService: PagerService,
        private formBuilder: FormBuilder
    ) {
    }

    ngOnInit() {
        this.apiUrl = "http://180.151.38.87:3000";
        this.hideSuccessFlash();
        this.hideErrorFlash();
        this.buildForm();
        this.initializeClassieJS();
        this.initializeViewModels();
        this.paginationModel = new PaginationModel();
        this.resources = new Resources();

        this.getCompaniesWithLocations();
        this.getAllBenchEmployees();
    }

    buildForm() {
        this.addEmployeeForm = this.formBuilder.group({
            name: ['', Validators.required],
            managerEmailId: ['', [Validators.required, Validators.pattern]],
            emailId: ['', [Validators.required, Validators.pattern]],
            skills: ['', Validators.required],
            company: ['', Validators.required],
            location: ['', Validators.required],
            phone: [],
            resume: [],
            availableBy: []
        });

        this.addEmployeeForm.valueChanges
            .subscribe(data => this.onValueChanged(data));

        this.onValueChanged(); // (re)set validation messages
    }

    onValueChanged(data?: any) {
        if (!this.addEmployeeForm) { return; }
        const form = this.addEmployeeForm;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    formErrors = {
        'name': '',
        'managerEmailId': '',
        'skills': '',
        'emailId': '',
        'company': '',
        'location': ''
    };

    validationMessages = {
        'name': {
            'required': 'Name is required.'
        },
        'managerEmailId': {
            'required': 'Email Id is required.',
            'pattern': 'Invalid Email Id.'
        },
        'skills': {
            'required': 'Skills is required.'
        },
        'emailId': {
            'required': 'Email Id is required.',
            'pattern': 'Invalid Email Id.'
        },
        'company': {
            'required': 'Company is required.'
        },
        'location': {
            'required': 'Location is required.'
        }
    };

    getCompaniesWithLocations() {
        this.homeService.getData('./assets/data/data.json').subscribe(data => {
            this.loadCompanyDDLs(data);
        },
            err => {
                this.showErrorFlash = true;
                this.errorMessage = this.resources.errorLoadingCompany;
                setTimeout(() => {
                    this.hideErrorFlash();
                }, 6000);
            },
            () => {
            });
    }

    loadLocationsBySearchCompany(companyName: string) {
        this.searchLocations = [];
        if (companyName != "") {
            let filteredLocations = this.searchCompanies.find(x => x.name == companyName);
            this.searchLocations = filteredLocations.locations;
        }
    }

    loadLocationsByCompany(companyName: string) {
        this.locations = [];
        if (companyName != "") {
            let filteredLocations = this.companies.find(x => x.name == companyName);
            this.locations = filteredLocations.locations;
        }
    }

    loadCompanyDDLs(data: any) {
        this.searchCompanies = data.companies;
        this.companies = data.companies;
    }

    getAllBenchEmployees() {
        this.homeService.getData(this.apiUrl + "/benchlist").subscribe(data => {
            this.benchEmployees = [];
            this.benchEmployees = data;
        },
            err => {
                this.showErrorFlash = true;
                this.errorMessage = this.resources.errorFetchEmployees;
                setTimeout(() => {
                    this.hideErrorFlash();
                }, 6000);
            },
            () => {
                this.setPage(1);
            });
    }

    initializeClassieJS() {
        (function () {
            // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
            if (!String.prototype.trim) {
                (function () {
                    // Make sure we trim BOM and NBSP
                    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function () {
                        return this.replace(rtrim, '');
                    };
                })();
            }

            [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
                // in case the input is already filled..
                if (inputEl.value.trim() !== '') {
                    classie.add(inputEl.parentNode, 'input--filled');
                }

                // events:
                inputEl.addEventListener('focus', onInputFocus);
                inputEl.addEventListener('blur', onInputBlur);
            });

            function onInputFocus(ev) {
                classie.add(ev.target.parentNode, 'input--filled');
            }

            function onInputBlur(ev) {
                if (ev.target.value.trim() === '') {
                    classie.remove(ev.target.parentNode, 'input--filled');
                }
            }
        })();
    }

    initializeViewModels() {
        this.availableByDateModel = new DatePickerModel();
        this.searchEmployeeViewModel = new SearchEmployeeViewModel();
        this.fileInputVariable.nativeElement.value = "";
    }

    fileEvent(fileInput: any) {
        this.addEmployeeForm.value.resume = this.fileInputVariable.nativeElement.files[0].name;
    }

    saveEmployeeDetails(event: any) {
        if (!this.addEmployeeForm.value.availableBy) {
            var datePipe = new DatePipe('en-us');
            this.addEmployeeForm.value.availableBy = new DateModel();
            this.addEmployeeForm.value.availableBy.formatted = datePipe.transform(new Date(), 'dd/MM/yyyy');
        }
        let employeeViewModel = [{
            "name": this.addEmployeeForm.value.name,
            "managerEmailId": this.addEmployeeForm.value.managerEmailId,
            "location": this.addEmployeeForm.value.location,
            "company": this.addEmployeeForm.value.company,
            "phone": this.addEmployeeForm.value.phone,
            "email": this.addEmployeeForm.value.emailId,
            "skills": this.addEmployeeForm.value.skills,
            "resume": this.fileInputVariable.nativeElement.files.length > 0 ?
                this.fileInputVariable.nativeElement.files[0].name : null,
            "availableBy": this.addEmployeeForm.value.availableBy.formatted
                ? this.addEmployeeForm.value.availableBy.formatted : ""
        }];
        this.homeService.saveEmployeeDetails(this.apiUrl + "/benchlist", employeeViewModel).subscribe(data => {
            if (this.fileInputVariable.nativeElement.files.length > 0) {
                console.log(data[0]._id);
                this.uploadFile(data[0]._id);
            }
        },
            err => {
                this.showErrorFlash = true;
                this.errorMessage = this.resources.errorSaveEmployee;
                setTimeout(() => {
                    this.hideErrorFlash();
                }, 6000);
            },
            () => {
                this.closeAddEmployeePopup();
                this.getAllBenchEmployees();
                this.showSuccessFlash = true;
                this.successMessage = this.resources.successSaveEmployee;
                setTimeout(() => {
                    this.hideSuccessFlash();
                }, 3000);
            });
    }

    hideSuccessFlash() {
        this.showSuccessFlash = false;
        this.successMessage = "";
    }

    hideErrorFlash() {
        this.showErrorFlash = false;
        this.errorMessage = "";
    }

    clear() {
        window.location.reload(true);
    }

    setPage(page: number) {
        if (page < 1 || page > this.paginationModel.pager.totalPages) {
            return;
        }

        if (this.benchEmployees && this.benchEmployees.length >= 0) {
            // get pager object from service
            this.paginationModel.pager = this.pagerService.getPager(this.benchEmployees.length, page);

            // calulating showing from and to results
            this.paginationModel.from = this.paginationModel.pager.startIndex + 1;
            this.paginationModel.to = this.paginationModel.pager.endIndex + 1;
            this.paginationModel.total = this.benchEmployees.length;

            // get current page of items
            this.paginationModel.pagedItems = [];
            this.paginationModel.pagedItems = this.benchEmployees.slice(this.paginationModel.pager.startIndex, this.paginationModel.pager.endIndex + 1);
        } else {
            this.paginationModel = new PaginationModel();
        }
    }

    searchEmployee() {
        this.showSpinner = true;
        let searchViewModel: any;

        if (!this.showHideAdvancedSearchDiv) {
            this.searchEmployeeViewModel.skills = this.technology;
        } else {
            this.searchEmployeeViewModel.skills = this.searchEmployeeViewModel.skills;
        }

        searchViewModel = [{
            "name": this.searchEmployeeViewModel.name,
            "skills": this.searchEmployeeViewModel.skills,
            "company": this.searchEmployeeViewModel.company,
            "location": this.searchEmployeeViewModel.location
        }];

        for (var i = 0; i < searchViewModel.length; i++) {
            if (this.searchEmployeeViewModel.name == "") {
                delete searchViewModel[i]['name'];
            }
            if (this.searchEmployeeViewModel.skills == "") {
                delete searchViewModel[i]['skills'];
            }
            if (this.searchEmployeeViewModel.company == "") {
                delete searchViewModel[i]['company'];
            }
            if (this.searchEmployeeViewModel.location == "") {
                delete searchViewModel[i]['location'];
            }
        }

        this.homeService.getEmployeeDetails(this.apiUrl + "/getbenchlist", searchViewModel).subscribe(data => {
            this.benchEmployees = [];
            this.benchEmployees = data;
        },
            err => {
                this.showSpinner = false;
                this.showErrorFlash = true;
                this.errorMessage = this.resources.errorSearchEmployees;
                setTimeout(() => {
                    this.hideErrorFlash();
                }, 6000);
            },
            () => {
                this.setPage(1);
                this.showSpinner = false;
            });
    }

    showAddEmployeePopup() {
        this.hideSuccessFlash();
        this.initializeViewModels();
        this.initializeClassieJS();
        this.buildForm();
        this.addEmployeeModal.show();
    }

    closeAddEmployeePopup() {
        this.hideSuccessFlash();
        this.initializeViewModels();
        this.initializeClassieJS();
        this.buildForm();
        this.addEmployeeModal.hide();
    }

    refreshParentBenchEmployeesList() {
        this.getAllBenchEmployees();
        this.successModal.show();
        setTimeout(() => {
            this.closeSuccessPopup();
        }, 3000);
    };

    closeSuccessPopup() {
        this.successModal.hide();
    };

    uploadFile(id: string) {
        this.filesToUpload = this.fileInputVariable.nativeElement.files;
        this.homeService.uploadFile(this.apiUrl + "/upload", id, this.filesToUpload).then(function (result) {
            console.log(result);
        }, function (error) {
            console.error(error);
        });
    };
}
