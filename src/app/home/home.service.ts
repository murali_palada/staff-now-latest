import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptionsArgs, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable, Subscriber } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HomeService {

    constructor(private http: Http
    ) {

    }

    uploadFile(url: string, id: string, files: any) {
        return new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("uploads", files[0]);
            formData.append("id", id);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", url + "/" + id, true);
            xhr.send(formData);
        });
    };

    getData(url: string): Observable<any> {
        return this.http.get(url)
            .map(data => {
                return data.json();
            });
    }

    post(url: string, body: Object) {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(url, bodyString, options) // ...using post request
            .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

    saveEmployeeDetails(url: string, body: Object): Observable<any> {
        return this.post(url, body);
    }

    getEmployeeDetails(url: string, body: Object): Observable<any> {
        return this.post(url, body);
    }

    saveInterestedManagerDetails(url: string, body: Object): Observable<any> {
        return this.post(url, body);
    }

    getInterestedManagerDetails(url: string, body: Object): Observable<any> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(url, body, options) // ...using post request
            .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

    sendEmail(url: string, body: Object): Observable<any> {
        return this.post(url, body);
    }
}