import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from "./home.routing";
import { HomeService } from "./home.service";
import { ModalModule } from 'ng2-bootstrap';
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule } from "@angular/forms";
import { PagerService } from './pager-service/pager.service';
import { DatePickerModule } from 'ng2-datepicker';
import { PopoverModule } from 'ngx-popover';
import { InterestedManagerPopoverComponent } from '../home/popovers/interested-manager-popover.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    FormsModule,
    DatePickerModule,
    PopoverModule
  ],
  providers: [HomeService, PagerService, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  declarations: [HomeComponent, InterestedManagerPopoverComponent]
})
export class HomeModule { }
