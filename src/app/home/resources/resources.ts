export class Resources {
    public errorLoadingCompany: string = "Error loading companies drop down.";
    public errorFetchEmployees: string = "Error while fetching all the employees.";
    public errorSaveEmployee: string = "Error while saving employee.";
    public errorSearchEmployees: string = "Error while searching employee(s).";
    public errorFetchIntrstdMngrDtls: string = "Error fetching interested manager details.";
    public errorSaveIntrtdManagrDtls: string = "Error saving interested manager details.";
    public successSaveEmployee: string = "Employee saved successfully.";
}